﻿using UnityEngine;

public class TargetFollower : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _maxOffset;
    private Transform _target;

    private bool IsOnTarget => Vector3.Distance(transform.position, _target.position) < 0.5f;

    public void Init(Transform target)
    {
        _target = target;
    }

    private void FixedUpdate()
    {
        if (IsOnTarget == false)
            MoveToTarget();
    }

    private void MoveToTarget()
    {
        transform.position = Vector3.Lerp(transform.position, _target.position, _speed * Time.fixedDeltaTime);
    }
}
