﻿using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _distance;
    private float _damage;
        
    private Vector3 Target => transform.position + transform.forward * _distance;

    public void PrepareToStart(Vector3 spawnPoint, Quaternion rotation)
    {
        transform.position = spawnPoint;
        transform.rotation = rotation;
    }

    public void Fire(float damage)
    {
        StopAllCoroutines();
        gameObject.SetActive(true);
        _damage = damage;
        StartCoroutine(MoveTo(Target));
    }

    private IEnumerator MoveTo(Vector3 target)
    {
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, _speed * Time.deltaTime);
            yield return null;
        }
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out DamageTakingBehaviour damageTaking))
        {
            damageTaking.ApplyDamage(_damage);
            gameObject.SetActive(false);
        }
    }
}
