﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float _damage;
    [SerializeField] private Bullet _bullet;
    [SerializeField] private Transform _bulletSpawnPoint;

    private BulletsPool _bulletsPool;

    private void Awake()
    {
        _bulletsPool = new BulletsPool(_bullet);
    }

    public void Shoot()
    {
        Bullet bullet = _bulletsPool.GetBullet();
        bullet.PrepareToStart(_bulletSpawnPoint.position, transform.rotation);
        bullet.Fire(_damage);
    }
}
