﻿using UnityEngine;

public enum PlayerType
{
    Human,
    Bot
}

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerType _playerType;
    public PlayerType PlayerType => _playerType;
}
