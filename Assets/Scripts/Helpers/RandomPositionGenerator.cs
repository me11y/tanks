﻿using UnityEngine;

public enum Direction
{
    Left,
    Right,
    Up,
    Down
}

public class RandomPositionGenerator
{
    private Camera _camera;

    private Vector3 BottomLeftCorner => _camera.ScreenToWorldPoint(Vector2.zero);
    private Vector3 TopLeftCorner => _camera.ScreenToWorldPoint(new Vector2(0, Screen.height));
    private Vector3 BottomRightCorner => _camera.ScreenToWorldPoint(new Vector2(Screen.width, 0));
    private Vector3 TopRightCorner => _camera.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

    public RandomPositionGenerator(Camera camera)
    {
        _camera = camera;
    }

    public Vector3 GetRandomPosition()
    {
        Direction spawnSide = (Direction)Random.Range(0, 4);
        switch (spawnSide)
        {
            case Direction.Left:
                return new Vector3(Random.Range(BottomLeftCorner.x - 5, BottomLeftCorner.x - 15), 0, Random.Range(BottomLeftCorner.z, TopLeftCorner.z));
            case Direction.Right:
                return new Vector3(Random.Range(BottomRightCorner.x + 5, BottomRightCorner.x + 15), 0, Random.Range(BottomRightCorner.z, TopRightCorner.z));
            case Direction.Up:
                return new Vector3(Random.Range(TopLeftCorner.x, TopRightCorner.x), 0, Random.Range(TopLeftCorner.z + 5, TopLeftCorner.z + 15));
            case Direction.Down:
                return new Vector3(Random.Range(BottomLeftCorner.x, BottomRightCorner.x), 0, Random.Range(BottomLeftCorner.z - 5, BottomLeftCorner.z - 15));
            default:
                return Vector3.zero;
        }
    }
}
