﻿using TMPro;
using UnityEngine;

public class HealthView : MonoBehaviour
{
    [SerializeField] private TMP_Text _health;
    [SerializeField] private DamageTakingBehaviour _damageTaking;

    private void OnEnable()
    {
        _damageTaking.HealthUpdated += UpdateView;
    }

    private void UpdateView(float health)
    {
        _health.text = health.ToString();
    }

    private void OnDisable()
    {
        _damageTaking.HealthUpdated -= UpdateView;
    }
}
