﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreenView : MonoBehaviour
{
    [SerializeField] private Button _restart;

    private void OnEnable()
    {
        _restart.onClick.AddListener(RestartScene);
    }

    private void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnDisable()
    {
        _restart.onClick.RemoveListener(RestartScene);
    }
}
