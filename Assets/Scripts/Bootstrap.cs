﻿using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private EnemiesSpawner _enemiesSpawner;
    [SerializeField] private TargetFollower _camera;
    [SerializeField] private GameObject _player;
    [SerializeField] private Transform _playerSpawnPosition;

    private GameObject _spawnedPlayer;

    private void OnValidate()
    {
        if (_player == null)
            return;

        if (CheckComponent<MovementBehaviour>(ref _player, nameof(_player)) == false)
            return;

        if (CheckComponent<WeaponSwitchingBehaviour>(ref _player, nameof(_player)) == false)
            return;
    }

    private bool CheckComponent<T>(ref GameObject target, string fieldName) where T : Component
    {
        if (target.GetComponent<T>() == null)
        {
            target = null;
            Debug.LogError($"Field {fieldName} requires component {typeof(T)}");
            return false;
        }
        return true;
    }

    private void Start()
    {
        _spawnedPlayer = Instantiate(_player, _playerSpawnPosition.position, Quaternion.identity);

        _camera.Init(_spawnedPlayer.transform);

        InitInput(); ;
        _enemiesSpawner.Init(_spawnedPlayer.transform);
    }

    private void InitInput()
    {
        GameObject input = new GameObject("Input");

        IMovementInput movementInput = input.AddComponent<KeyboardMovementInput>();
        IRotatingInput rotatingInput = input.AddComponent<KeyboardRotatingInput>();
        KeyboardWeaponsInput weaponsInput = input.AddComponent<KeyboardWeaponsInput>();

        _spawnedPlayer.GetComponent<MovementBehaviour>().Init(movementInput);
        _spawnedPlayer.GetComponent<RotatingBehaviour>().Init(rotatingInput);
        _spawnedPlayer.GetComponent<WeaponSwitchingBehaviour>().Init(weaponsInput);
        _spawnedPlayer.GetComponent<ShootingBehaviour>().Init(weaponsInput);   
    }
}
