﻿using System;

public interface IWeaponSwitchingInput
{
    event Action SwitchWeaponLeftPressed;
    event Action SwitchWeaponRightPressed;
}
