﻿using System;

public interface IRotatingInput
{
    event Action RotateLeftPressed;
    event Action RotateRightPressed;
}

