﻿using System;

public interface IShootingInput
{
    event Action ShootPressed;
}
