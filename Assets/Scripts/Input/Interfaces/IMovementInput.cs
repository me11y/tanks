﻿using System;

public interface IMovementInput
{
    event Action MoveFrontPressed;
}
