﻿using System;
using System.Collections;
using UnityEngine;

public class BotInput : MonoBehaviour, IMovementInput, IMeleeInput
{
    private Transform _target;
    private bool _needToMove = true;
    private Player _self;

    private readonly float _cooldownPeriod = 1; 
    private bool _cooldownActivated = false;

    public event Action MoveFrontPressed;
    public event Action AttackPressed;

    private void Awake()
    {
        _self = GetComponent<Player>();
    }

    public void Init(Transform target)
    {
        _target = target;
    }

    private void FixedUpdate()
    {
        transform.LookAt(_target, Vector3.up);
        if (_needToMove == true)
            MoveFrontPressed?.Invoke();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (IsCollisionMustBeAttacked(collision))
        {
            _needToMove = false;
            PressAttack();
        }
    }

    private void PressAttack()
    {
        if (_cooldownActivated == false)
        {
            AttackPressed?.Invoke();
            StartCoroutine(WaitForCoolDown());
        }
    }

    private IEnumerator WaitForCoolDown()
    {
        _cooldownActivated = true;
        yield return new WaitForSeconds(_cooldownPeriod);
        _cooldownActivated = false;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (IsCollisionMustBeAttacked(collision))
        {
            _needToMove = true;
        }
    }

    private bool IsCollisionMustBeAttacked(Collision collision)
    {
        return collision.gameObject.TryGetComponent(out Player player) && player.PlayerType != _self.PlayerType;
    }
}
