﻿using System;
using UnityEngine;

public class KeyboardMovementInput : MonoBehaviour, IMovementInput
{
    public event Action MoveFrontPressed;

    private void FixedUpdate()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetKey(KeyCode.W))
            MoveFrontPressed?.Invoke();
    }
}
