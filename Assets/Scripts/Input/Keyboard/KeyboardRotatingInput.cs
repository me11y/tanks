﻿using System;
using UnityEngine;

public class KeyboardRotatingInput : MonoBehaviour, IRotatingInput
{
    public event Action RotateLeftPressed;
    public event Action RotateRightPressed;

    private void FixedUpdate()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetKey(KeyCode.A))
        {
            RotateLeftPressed?.Invoke();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            RotateRightPressed?.Invoke();
        }
    }
}

