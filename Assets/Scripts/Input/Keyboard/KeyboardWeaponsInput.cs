﻿using System;
using UnityEngine;

public class KeyboardWeaponsInput : MonoBehaviour, IWeaponSwitchingInput, IShootingInput
{
    public event Action ShootPressed;
    public event Action SwitchWeaponLeftPressed;
    public event Action SwitchWeaponRightPressed;

    private void Update()
    {
        HandleInput();   
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            ShootPressed.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchWeaponLeftPressed.Invoke();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            SwitchWeaponRightPressed.Invoke();
        }
    }
}
