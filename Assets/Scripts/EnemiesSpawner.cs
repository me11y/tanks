﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private List<BotInput> _enemies;
    [SerializeField] private int _maxEnemiesSpawned;
    private Transform _target;
    private RandomPositionGenerator _randomPositionGenerator;

    private readonly List<BotInput> _spawnedEnemies = new List<BotInput>();

    public void Init(Transform target)
    {
        _target = target;
        _randomPositionGenerator = new RandomPositionGenerator(_camera);
        StartCoroutine(SpawnEnemies());
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            if (_spawnedEnemies.Count == _maxEnemiesSpawned)
            {
                yield return null;
                continue;
            }

            BotInput enemy = SpawnEnemy();
            if(enemy == null)
            {
                yield return null;
                continue;
            }
            _spawnedEnemies.Add(enemy);
            enemy.GetComponent<DamageTakingBehaviour>().Died += DestroyEnemy;
            yield return new WaitForSeconds(1);
        }
    }

    private BotInput SpawnEnemy()
    {
        Vector3 randomPosition = _randomPositionGenerator.GetRandomPosition();

        randomPosition.y = _target.position.y;

        if(CanSpawnOnPosition(randomPosition) == false)
        {
            return null;
        }

        BotInput enemy = Instantiate(GetRandomEnemyPrefab());
        enemy.Init(_target);
        enemy.GetComponent<MovementBehaviour>().Init(enemy);
        enemy.GetComponent<MeleeBehaviour>().Init(enemy);
        enemy.transform.position = randomPosition;

        return enemy;
    }

    private bool CanSpawnOnPosition(Vector3 position)
    {
        Ray ray = new Ray(position + Vector3.up, Vector3.down);
        if (Physics.Raycast(ray, 10))
        {
            return true;
        }

        return false;
    }

    private BotInput GetRandomEnemyPrefab()
    {
        return _enemies[Random.Range(0, _enemies.Count)];
    }

    private void DestroyEnemy(DamageTakingBehaviour damageTakingBehaviour)
    {
        BotInput died = _spawnedEnemies.Find((enemy) => enemy.GetComponent<DamageTakingBehaviour>() == damageTakingBehaviour);
        _spawnedEnemies.Remove(died);
        Destroy(died.gameObject);
    }

}
