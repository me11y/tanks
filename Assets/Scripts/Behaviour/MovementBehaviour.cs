﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(SurfaceSlider))]
public class MovementBehaviour : MonoBehaviour
{
    [SerializeField] private float _speed;
    private Rigidbody _self;
    private SurfaceSlider _surface;

    private IMovementInput _input;

    private void OnValidate()
    {
        if (_speed < 0)
        {
            _speed = 0;
        }
    }

    private void Awake()
    {
        _self = GetComponent<Rigidbody>();
        _surface = GetComponent<SurfaceSlider>();
    }

    private void OnEnable()
    {
        if (_input != null)
            SubscribeOnInput();
    }

    public void Init(IMovementInput input)
    {
        _input = input;
        SubscribeOnInput();
    }

    private void SubscribeOnInput()
    {
        _input.MoveFrontPressed += MoveFront;
    }

    private void MoveFront()
    {
        Vector3 directiongAlongSurface = _surface.Project(transform.forward);
        Vector3 offset = directiongAlongSurface * (_speed * Time.deltaTime);

        _self.MovePosition(_self.position + offset);
    }


    private void OnDisable()
    {
        _input.MoveFrontPressed -= MoveFront;
    }
}
