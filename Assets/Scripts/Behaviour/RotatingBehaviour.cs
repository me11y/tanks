﻿using UnityEngine;

public class RotatingBehaviour : MonoBehaviour
{
    private IRotatingInput _input;

    private readonly float _rotatingSpeed = 200;

    private void OnEnable()
    {
        if (_input != null)
            SubscribeOnInput();
    }

    public void Init(IRotatingInput input)
    {
        _input = input;
        SubscribeOnInput();
    }

    private void SubscribeOnInput()
    {
        _input.RotateLeftPressed += RotateLeft;
        _input.RotateRightPressed += RotateRight;
    }

    private void RotateLeft()
    {
        transform.Rotate(Vector3.up, -Time.deltaTime * _rotatingSpeed);
    }

    private void RotateRight()
    {
        transform.Rotate(Vector3.up, Time.deltaTime * _rotatingSpeed);
    }

    private void OnDisable()
    {
        _input.RotateLeftPressed -= RotateLeft;
        _input.RotateRightPressed -= RotateRight;
    }
}
