﻿using System;
using UnityEngine;

public class DamageTakingBehaviour : MonoBehaviour
{
    [SerializeField] private float _health;

    [Range(0, 1)]
    [SerializeField] private float _defense;

    private float DamagePercent => 1 - _defense;

    public event Action<float> HealthUpdated;
    public event Action<float> DamageTaken;
    public event Action<DamageTakingBehaviour> Died;

    private void Start()
    {
        HealthUpdated?.Invoke(_health);
    }

    public void ApplyDamage(float damage)
    {
        if(enabled == false)
        {
            return;
        }

        float amount = CalculateDamage(damage);
        _health -= amount;

        if (_health < 0)
            _health = 0;

        HealthUpdated?.Invoke(_health);
        DamageTaken?.Invoke(amount);

        if (_health == 0)
            Died?.Invoke(this);
    }

    private float CalculateDamage(float damage)
    {
        return damage * DamagePercent;
    }

    private void OnDestroy()
    {
        DamageTaken = null;
        Died = null;
    }
}
