﻿using UnityEngine;

[RequireComponent(typeof(IWeaponStorage))]
public class ShootingBehaviour : MonoBehaviour
{
    private IWeaponStorage _weapons;
    private IShootingInput _input;

    private void Awake()
    {
        _weapons = GetComponent<IWeaponStorage>();
    }

    private void OnEnable()
    {
        if (_input != null)
            SubscribeOnInput();
    }

    public void Init(IShootingInput input)
    {
        _input = input;
        SubscribeOnInput();
    }

    private void SubscribeOnInput()
    {
        _input.ShootPressed += Shoot;
    }

    private void Shoot()
    {
        _weapons.CurrentWeapon.Shoot();
    }

    private void OnDisable()
    {
        _input.ShootPressed -= Shoot;
    }
}
