﻿using UnityEngine;

[RequireComponent(typeof(DamageTakingBehaviour))]
public class GameOverBehaviour : MonoBehaviour
{
    [SerializeField] private GameOverScreenView _gameOverCanvas;
    private DamageTakingBehaviour _damageTakingBehaviour;

    private void Awake()
    {
        _damageTakingBehaviour = GetComponent<DamageTakingBehaviour>();
    }

    private void OnEnable()
    {
        _damageTakingBehaviour.Died += ShowGameOver;
    }

    private void ShowGameOver(DamageTakingBehaviour target)
    {
        Instantiate(_gameOverCanvas);
        _damageTakingBehaviour.enabled = false;
        enabled = false;
    }

    private void OnDisable()
    {
        _damageTakingBehaviour.Died -= ShowGameOver;
    }
}
