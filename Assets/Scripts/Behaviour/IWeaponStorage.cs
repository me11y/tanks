﻿public interface IWeaponStorage
{
    Weapon CurrentWeapon { get; }
}

