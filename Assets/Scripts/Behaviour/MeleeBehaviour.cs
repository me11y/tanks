﻿using UnityEngine;

[RequireComponent(typeof(Player))]
public class MeleeBehaviour : MonoBehaviour
{
    [SerializeField] private float _damage;

    private Player _self;
    private IMeleeInput _input;
    private DamageTakingBehaviour _target;

    private bool IsTargetAlly => _target.GetComponent<Player>().PlayerType == _self.PlayerType;

    private void Awake()
    {
        _self = GetComponent<Player>();
    }

    private void OnEnable()
    {
        if (_input != null)
            SubscribeOnInput();
    }

    public void Init(IMeleeInput input)
    {
        _input = input;
        SubscribeOnInput();
    }

    private void SubscribeOnInput()
    {
        _input.AttackPressed += Attack;
    }

    private void Attack()
    {
        if (_target == null)
            return;

        if (IsTargetAlly)
            return;

        _target.ApplyDamage(_damage);
    }


    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.TryGetComponent(out _target);
    }

    private void OnCollisionExit(Collision collision)
    {
        if(_target != null && _target == collision.gameObject.GetComponent<DamageTakingBehaviour>())
        {
            _target = null;
        }
    }

    private void OnDisable()
    {
        _input.AttackPressed -= Attack;
    }

}
