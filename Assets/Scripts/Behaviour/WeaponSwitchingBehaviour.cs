﻿using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitchingBehaviour : MonoBehaviour, IWeaponStorage
{
    [SerializeField] private List<Weapon> _weapons;
    [SerializeField] private Transform _weaponSpawnPoint;
    private int _currentWeaponIndex = 0;
    private IWeaponSwitchingInput _input;

    public Weapon CurrentWeapon { get; private set; }

    private void OnEnable()
    {
        if(_input != null)
            SubscribeOnInput();
    }

    public void Init(IWeaponSwitchingInput input)
    {
        _input = input;
        SubscribeOnInput();
        SwitchWeapon(_weapons[_currentWeaponIndex]);
    }

    private void SubscribeOnInput()
    {
        _input.SwitchWeaponLeftPressed += SwitchWeaponLeft;
        _input.SwitchWeaponRightPressed += SwitchWeaponRight;
    }

    private void SwitchWeaponLeft()
    {
        _currentWeaponIndex--;
        if(_currentWeaponIndex < 0)
        {
            _currentWeaponIndex = _weapons.Count - 1;
        }

        SwitchWeapon(_weapons[_currentWeaponIndex]);
    }

    private void SwitchWeaponRight()
    {
        _currentWeaponIndex++;
        if (_currentWeaponIndex >= _weapons.Count)
        {
            _currentWeaponIndex = 0;
        }

        SwitchWeapon(_weapons[_currentWeaponIndex]);
    }

    private void SwitchWeapon(Weapon weapon)
    {
        if (CurrentWeapon != null)
        {
            Destroy(CurrentWeapon.gameObject);
        }

        CurrentWeapon = Instantiate(weapon, transform);
        CurrentWeapon.transform.position = _weaponSpawnPoint.position;
    }

    private void OnDisable()
    {
        _input.SwitchWeaponLeftPressed -= SwitchWeaponLeft;
        _input.SwitchWeaponRightPressed -= SwitchWeaponRight;
    }
}
